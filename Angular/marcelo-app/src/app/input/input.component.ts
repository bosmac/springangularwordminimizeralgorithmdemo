import { Component, OnInit } from '@angular/core';
import { RestEndpointService } from '../rest-endpoint.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  constructor(private restEndpointService : RestEndpointService,
              private router : Router) 
  { }

  ngOnInit() { }

  onClickSubmitButton(string: String)
  {
    console.log("clicked " + string);
    this.restEndpointService.getMinimizedString(string).subscribe(data => {
      console.log(data);
      this.restEndpointService.setInput(string);
      this.restEndpointService.setOutput(data);
      this.router.navigate(['/result']);
    });
  }

}
