import { Component, OnInit, Input } from '@angular/core';
import { RestEndpointService } from '../rest-endpoint.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  public originalMessage : String;
  public minimizedMessage : String;
  
  constructor(private restEndpointService : RestEndpointService) { }

  ngOnInit() {
    this.originalMessage = this.restEndpointService.getInput();
    this.minimizedMessage = this.restEndpointService.getOutput();
  }

}
