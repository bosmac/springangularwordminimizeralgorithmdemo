import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestEndpointService {

  private input : String;
  private output : any;

  constructor(private http: HttpClient) { }

  public getGreeting() : Observable < any > {
    return this.http.get<any>('http://0.0.0.0:8080/helloworld');
  }
  
  public getMinimizedString(input : String) : Observable < any > {
    return this.http.post<any>('http://0.0.0.0:8080/minimizer', input);
  }

  public setInput(input : String)
  { 
    this.input = input;
  }

  public getInput() : String
  {
    return this.input;
  }

  public setOutput(output : any)
  { 
    this.output = output;
  }

  public getOutput() : any
  {
    return this.output;
  }

}
