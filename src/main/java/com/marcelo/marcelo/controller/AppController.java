package com.marcelo.marcelo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.marcelo.minimizer.Minimizer;

@RestController
@CrossOrigin
public class AppController {

    @RequestMapping( value = "/helloworld", method = { RequestMethod.GET, RequestMethod.POST })
    public Minimizer helloWorld() {
    	Minimizer mini = new Minimizer();
    	mini.setOriginalString("Hello World");
    	mini.setMinimizedString("HW");
        return mini;
    }
    
    @RequestMapping( value = "/minimizer", method = { RequestMethod.GET, RequestMethod.POST } )
    public Minimizer minimizer(@RequestBody String input) {
    	Minimizer mini = new Minimizer();
    	mini.performMinimization(input);
        return mini;
    }
}