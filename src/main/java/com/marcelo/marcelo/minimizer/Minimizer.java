package com.marcelo.marcelo.minimizer;

import java.util.ArrayList;
import java.util.HashMap;

public class Minimizer {
	
	public String originalString;
	public String minimizedString;
	java.util.HashMap<String, WordTracker> map = new HashMap<>();

	public Minimizer(){ }
	
	public String getOriginalString() {
		return originalString;
	}

	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}

	public String getMinimizedString() {
		return minimizedString;
	}

	public void setMinimizedString(String minimizedString) {
		this.minimizedString = minimizedString;
	}

	public void performMinimization(String input)
	{
		if(input != null)
		{
			input = input.replace(",", "");
			input = input.replace(".", "");
			this.originalString = input.toLowerCase();
			String[] words = originalString.split(" ");
			ArrayList<String> result = new ArrayList<>();
			for(int i=0; i<words.length; i++){
				if(!map.containsKey(words[i])){
					map.put(words[i], new WordTracker(i));
					result.add(words[i]);
				}else{
					WordTracker wt = map.get(words[i]);
					wt.incrementNumberOfOccurences();
					wt.addIndexOfSubOccurences(i);
					int firstIndex = wt.getIndexOfFirstOccurence();
					map.put(words[i], wt);
					result.add("$"+firstIndex);
				}
			}
			
			String temp = "";
			for(String s : result){
				temp = temp + s + " ";
			}
			this.minimizedString = temp.trim(); 
		}
		else
		{
			throw new IllegalStateException("Unable to proceed...");
		}
	}

}
