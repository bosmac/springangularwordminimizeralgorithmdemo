package com.marcelo.marcelo.minimizer;

import java.util.ArrayList;
import java.util.List;

public class WordTracker {

	private int indexOfFirstOccurence;
	private int numberOfOccurences;
	List<Integer> indexOfSubOccurences;
	
	public WordTracker(int indexOfFirstOccurence){
		this.indexOfFirstOccurence = indexOfFirstOccurence;
		this.numberOfOccurences = 1;
		this.indexOfSubOccurences = new ArrayList<>();
	}
	
	public int getIndexOfFirstOccurence() {
		return indexOfFirstOccurence;
	}

	public void setIndexOfFirstOccurence(int indexOfFirstOccurence) {
		this.indexOfFirstOccurence = indexOfFirstOccurence;
	}

	public int getNumberOfOccurences() {
		return numberOfOccurences;
	}

	public void setNumberOfOccurences(int numberOfOccurences) {
		this.numberOfOccurences = numberOfOccurences;
	}

	public List<Integer> getIndexOfSubOccurences() {
		return indexOfSubOccurences;
	}

	public void setIndexOfSubOccurences(List<Integer> indexOfSubOccurences) {
		this.indexOfSubOccurences = indexOfSubOccurences;
	}

	public void addIndexOfSubOccurences(int index){
		indexOfSubOccurences.add(index);
	}
	
	public void incrementNumberOfOccurences(){
		this.numberOfOccurences++;
	}
}
