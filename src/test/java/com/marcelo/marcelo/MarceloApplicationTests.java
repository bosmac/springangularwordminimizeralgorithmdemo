package com.marcelo.marcelo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.marcelo.marcelo.minimizer.Minimizer;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MarceloApplicationTests {
	
	@Test
	public void contextLoads() {
		String input = "Hello World Hello World";
		String output = "hello world $0 $1";
		Minimizer mini = new Minimizer();
		mini.performMinimization(input);
		String minimized = mini.getMinimizedString();
		assertThat(minimized).isEqualToIgnoringNewLines(output);
	}

}
